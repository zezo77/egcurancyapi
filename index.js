const PORT = process.env.PORT || 8000;
const baseURL = "https://egrates.com";
const express = require("express");
const axios = require("axios");
const cheerio = require("cheerio");
const mcache = require("memory-cache");
const apiVersion = "v1";

const app = express();

var cache = (duration) => {
  return (req, res, next) => {
    let key = "__express__" + req.originalUrl || req.url;
    let cachedBody = mcache.get(key);
    if (cachedBody) {
      res.contentType("application/json");
      res.send(cachedBody);
      return;
    } else {
      res.sendResponse = res.send;
      res.send = (body) => {
        mcache.put(key, body, duration * 1000);
        res.sendResponse(body);
      };
      next();
    }
  };
};

function toTimestamp(strDate) {
  var datum = Date.parse(strDate);
  return datum / 1000;
}

// endpoints

app.get('/', (req, res) => {
  res.json("welcome to egc API");
});

app.get(`/${apiVersion}/currency/:currency?`, cache(900), (req, res) => {
  let currency = req.params.currency || 'USD';
  axios
    .get(baseURL + "/currency/" + currency)
    .then((response) => {
      let currency = [];
      const html = response.data;
      const $ = cheerio.load(html);
      $("table.tbl-curr > tbody > tr").each( function () {
        const id = parseInt($(this).find("td:eq(0) a").attr("href").split("/").pop());
        const bank = $(this).find("td:eq(0) img").attr("title");
        const buy = $(this).find("td:eq(1) i").attr("data-value");
        const sell = $(this).find("td:eq(2) i").attr("data-value");
        const updated_at = toTimestamp(
          $(this).find("td:eq(3) time:eq(1)").attr("datetime")
        );

        currency.push({
          id,
          bank,
          buy,
          sell,
          updated_at,
        });
      });
      res.json(currency);
    })
    .catch((err) => console.log(err));
});

app.listen(PORT, () => {
  console.log(`server running on port ${PORT}`);
});
