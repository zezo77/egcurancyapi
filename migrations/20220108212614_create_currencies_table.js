
exports.up = function(knex) {
    return knex.schema.createTable('currencies', function (table) {
        table.increments();
        table.string('country', 100);
        table.string('currency', 100);
        table.string('code', 4).notNullable();
        table.smallint('minor_unit');
        table.string('symbol', 100);
    })
};

exports.down = function(knex) {
    return knex.schema.dropTableIfExists('currencies')  
};
