
exports.up = function(knex) {
    return knex.schema.createTable('banks', function (table) {
        table.increments();
        table.string('ar_name');
        table.string('en_name');
        table.string('code', 4).notNullable();
        table.timestamps(true, true);
    })
};

exports.down = function(knex) {
    return knex.schema.dropTableIfExists('banks')  
};
